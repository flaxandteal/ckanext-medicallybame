# ckanext-medicallybame

This is the main repo for the MedicallyBAME data portal. All of the CKAN customizations are added
in this extension.

## Getting Started

### Requirements

This extension works with CKAN 2.8.x.

### Installation

To install ckanext-medicallybame:

1. Activate your CKAN virtual environment, for example:

    ```sh
    . /usr/lib/ckan/default/bin/activate
    ```

1. Install the ckanext-medicallybame Python package into your virtual environment:

    ```sh
    pip install ckanext-medicallybame
    ```

1. Add `medicallybame` to the `ckan.plugins` setting in your CKAN config file (by default
   the config file is located at `/etc/ckan/default/production.ini`).

1. Restart CKAN. For example if you've deployed CKAN with Apache on Ubuntu::

    ```sh
    sudo service apache2 reload
    ```

## Config Settings

...

## Development

### Development Installation

To install ckanext-medicallybame for development, activate your CKAN virtualenv and do:

```sh
  git clone https://github.com/dkayee/ckanext-medicallybame.git
  cd ckanext-medicallybame
  python setup.py develop
  pip install -r requirements-dev.txt
```

All code MUST follow [PEP8 Style Guide](https://www.python.org/dev/peps/pep-0008/). Most editors have
plugins or integrations and automatic checking for PEP8 compliance so make sure you use them.

### Running the Tests

To run the tests, do:

```sh
  pytest --ckan-ini=test.ini
```

To run the tests and produce a coverage report, first make sure you have `pytest-cov` installed in your
virtualenv (`pip install pytest-cov`) then run:

```sh
  pytest --ckan-ini=test.ini --cov=ckanext.medicallybame
```
