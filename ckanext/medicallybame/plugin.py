import inspect
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.lib.helpers as ckan_helpers
from ckan.lib.plugins import DefaultTranslation

from ckanext.medicallybame import routing
from ckanext.medicallybame import helpers as medbame_helpers
from ckanext.medicallybame.logic import setup_action_functions, setup_auth_functions


class MedicallyBAMEPlugin(plugins.SingletonPlugin, DefaultTranslation):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IAuthFunctions)
    plugins.implements(plugins.IRoutes, inherit=True)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.ITranslation)
    plugins.implements(plugins.IActions)

    # IActions

    def get_actions(self):
        return setup_action_functions()

    # IAuthFunctions

    def get_auth_functions(self):
        return setup_auth_functions()

    # ITemplateHelpers

    def _is_helper(self, member):
        return callable(member) \
            and getattr(member, '__name__', None) \
            and member.__name__[0] != '_'

    def get_helpers(self):
        members = inspect.getmembers(medbame_helpers, self._is_helper)
        helpers = {'get_action': ckan_helpers.get_action}  # CKAN helpers
        helpers.update(members)
        return helpers

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'medicallybame')

    def update_config_schema(self, schema):
        """Extends the default CKAN config schema to contain settings
        specific to MedicallyBAME.
        """
        ignore_missing = toolkit.get_validator('ignore_missing')
        unicode_safe = toolkit.get_validator('unicode_safe')
        validators = [ignore_missing, unicode_safe]

        for idx in range(1, 5):
            schema.update({
                'medicallybame_bg_image_url_%i' % idx: validators,
                'medicallybame_bg_image_upload_%i' % idx: validators,
                'medicallybame_clear_bg_image_upload_%i' % idx: validators,
            })

        schema.update({'medicallybame_dataset_description': validators})
        return schema

    # IRoutes

    def before_map(self, routing_map):
        router = routing.BAMERouter(routing_map)
        router.set_routes()
        return routing_map
