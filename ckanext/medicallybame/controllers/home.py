import logging
import smtplib

from ckan import logic, model
from ckan.common import c
from ckan.controllers.home import HomeController as Controller
from ckan.lib.navl import dictization_functions as df
from ckan.lib import helpers as h
from ckan.plugins import toolkit


clean_dict = logic.clean_dict
parse_params = logic.parse_params
tuplize_dict = logic.tuplize_dict

log = logging.getLogger(__name__)


class HomeController(Controller):
    def index(self):
        c.groups = self._list_groups()
        c.sorted_groups = sorted(c.groups, key=lambda x: x['display_name'].lower())
        return super(HomeController, self).index()

    def contact(self, data=None, errors=None, error_summary=None):
        context = {'user': c.user, 'auth_user_obj': c.userobj}

        if toolkit.request.method == 'POST':
            data_dict = clean_dict(
                df.unflatten(tuplize_dict(parse_params(toolkit.request.params)))
            )

            try:
                action = toolkit.get_action('contactus_send')
                action(context, data_dict)

                h.flash_success('Message sent successfully.')
            except logic.ValidationError as ex:
                errors = ex.error_dict
                error_summary = ex.error_summary
            except smtplib.SMTPServerDisconnected as ex:
                h.flash_error(str(ex))
                log.exception(ex)

        # display form
        data = data or {}
        errors = errors or {}
        error_summary = error_summary or {}
        extra_vars = {'data': data, 'errors': errors, 'error_summary': error_summary}
        return toolkit.render('home/contact.html', extra_vars)

    def _list_groups(self):
        context = {'model': model, 'session': model.Session, 'user': c.user or c.author}
        data_dict = {'all_fields': True, 'type': 'group', 'limit': None, 'offset': 0}
        return logic.get_action('group_list')(context, data_dict)
