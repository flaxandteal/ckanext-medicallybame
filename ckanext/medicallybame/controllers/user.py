import json

from ckan import logic, model
from ckan.common import _, c, request
from ckan.controllers.user import UserController as Controller
from ckan.lib import base
from ckan.lib import helpers as h
from ckan.lib import search
from ckan.views.user import index as user_index

check_access = logic.check_access
NotAuthorized = logic.NotAuthorized


class UserController(Controller):

    def _authorize(self, sysadmin_required=False):
        if sysadmin_required and not self._current_user_is_sysadmin():
            return h.redirect_to('home')
        else:
            context = {'model': model, 'session': model.Session,
                       'user': c.user or c.author, 'auth_user_obj': c.userobj,
                       'save': 'save' in request.params}
            try:
                check_access('package_create', context)
                return True
            except NotAuthorized:
                base.abort(401, _('Unauthorized to change config'))

    def index(self):
        if not (c.user and c.userobj and c.userobj.sysadmin):
            h.redirect_to('/')

        # NOTE: used user_index in place of super(UserController, self).index()
        # as base template relies on how params are provided in user_index plus
        # user_index is preferred over the later method as CKAN transitions to
        # Flask blueprints
        return user_index()

    def drafts(self):
        self._authorize()
        # /ckan/lib/default/src/ckan/ckan/logic/action/get.py #1937
        query_params = {
            'sort': 'score desc, metadata_modified desc',
            'fq': '+state:(draft OR active)',
            'rows': 1000,
            'fl': 'id validated_data_dict'
        }

        query = search.query_for(model.Package)
        query.run(query_params)
        results = []
        for package in query.results:
            package_dict = package.get('validated_data_dict')

            if package_dict:
                package_dict = json.loads(package_dict)
                results.append(package_dict)

        if self._current_user_is_sysadmin():
            draft_packages = [
                package for package in results
                if package['private'] or package['state'] == 'draft'
            ]
        else:
            roles = self._roles_by_organization()
            draft_packages = [
                package for package in results
                if (
                    (package['private'] or package['state'] == 'draft')
                    and roles[package['organization']['name']][c.user]
                )
            ]

        extra_vars = {'draft_packages': draft_packages}
        return base.render('package/drafts.html', extra_vars=extra_vars)

    @staticmethod
    def _current_user_is_sysadmin():
        context = {'model': model, 'user': c.user, 'auth_user_obj': c.userobj}
        try:
            logic.check_access('sysadmin', context, {})
            return True
        except logic.NotAuthorized:
            return False

    @staticmethod
    def _roles_by_organization():
        orgs_and_users = {}
        all_users = model.Session.query(model.User).filter_by(state='active')
        for org in model.Session.query(model.Group).filter_by(state='active'):
            orgs_and_users[org.name] = {}
            users_id_in_org = UserController.get_organization_users_ids(org)
            for user in all_users:
                if user.id in users_id_in_org:
                    orgs_and_users[org.name][user.name] = True
                else:
                    orgs_and_users[org.name][user.name] = None
        return orgs_and_users

    @staticmethod
    def get_organization_users_ids(org):
        return [
            m.table_id for m in org.member_all
            if UserController.organization_member_is_editor(m)
        ]

    @staticmethod
    def organization_member_is_editor(member):
        return (
            member.table_name == 'user' and
            member.state == 'active' and
            member.capacity == 'editor'
        )
