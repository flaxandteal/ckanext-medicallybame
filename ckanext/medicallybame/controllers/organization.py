from ckan.common import c
from ckan.lib import helpers as h
from ckan.controllers.organization import OrganizationController as Controller


class OrganizationController(Controller):

    group_types = ['organization']

    def index(self):
        if not (c.user and c.userobj and c.userobj.sysadmin):
            h.redirect_to('/')
        return super(OrganizationController, self).index()
