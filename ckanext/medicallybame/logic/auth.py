def contactus_send(context, data_dict):
    """Allow any user to send message via the contact us form.
    """
    return {'success': True}


def setup_auth_functions():
    return {
        "contactus_send": contactus_send
    }
