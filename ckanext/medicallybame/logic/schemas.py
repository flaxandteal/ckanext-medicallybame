from ckan.plugins import toolkit

# validators
email_validator = toolkit.get_validator('email_validator')
ignore_missing = toolkit.get_validator('ignore_missing')
not_empty = toolkit.get_validator('not_empty')
unicode_safe = toolkit.get_validator('unicode_safe')


def contact_form_schema():
    return {
        'name': [unicode_safe, not_empty],
        'email': [unicode_safe, not_empty, email_validator],
        'message': [unicode_safe, not_empty],
        'subject': [unicode_safe, ignore_missing],
    }
