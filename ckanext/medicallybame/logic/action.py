import logging
from ckan import logic, model
from ckan.common import config
from ckan.lib.base import render_jinja2
from ckan.lib import helpers as h
from ckan.lib import mailer
from ckan.lib import uploader
from ckan.plugins import toolkit
from ckan.lib.navl import dictization_functions as df

from ckanext.medicallybame.logic import schemas
from ckanext.medicallybame.helpers.content import BANNER_COUNT

log = logging.getLogger(__name__)


def contactus_send(context, data_dict):
    """Sends the contact us message to sysadmin users for the portal.

    :param name: name of the person dropping contact message
    :type name: string
    :param email: email of person dropping contact message
    :type email: string (email)
    :param subject: subject of contact message
    :type subject: string
    :param message: message being dropped
    :type message: string
    """
    logic.check_access('contactus_send', context)

    schema = schemas.contact_form_schema()
    data, errors = df.validate(data_dict, schema, context)
    if errors:
        raise logic.ValidationError(errors)

    site_title = config.get('ckan.site_title')
    subject = '{}: New message from contact form'.format(site_title)

    # get list of all sysadmin users to send message to
    sysadmins = (
        model.Session.query(model.User)
        .filter_by(sysadmin=True, state=model.State.ACTIVE)
        .all()
    )

    template_vars = data.copy()
    log.debug(template_vars)

    for recipient in sysadmins:
        template_vars['user'] = recipient
        body = render_jinja2('emails/contact-us.txt', template_vars)

        try:
            mailer.mail_user(recipient, subject, body)
        except mailer.MailerException as ex:
            log.error(ex)


@toolkit.chained_action
def config_option_update(action, context, data_dict):
    """Override a core CKAN action by the same name which allows modification of
    some runtime-editable config options.
    """
    for idx in range(1, BANNER_COUNT + 1):
        bg_image_key = 'medicallybame_bg_image_url_%i' % idx

        # handle background image upload
        upload = uploader.get_uploader('admin')
        upload.update_data_dict(data_dict, bg_image_key,
                                'medicallybame_bg_image_upload_%i' % idx,
                                'medicallybame_clear_bg_image_upload_%i' % idx)

        upload.upload(uploader.get_max_image_size())

        # process background image url to set full url
        if bg_image_key in data_dict and data_dict[bg_image_key]:
            value = data_dict[bg_image_key]
            if not (value.startswith('http') or value.startswith('/')):
                image_path = 'uploads/admin/'
                value = h.url_for_static('{0}{1}'.format(image_path, value))

                # assign full url to key
                data_dict[bg_image_key] = value

    # call core/original action
    return action(context, data_dict)


def setup_action_functions():
    return {
        "config_option_update": config_option_update,
        "contactus_send": contactus_send,
    }
