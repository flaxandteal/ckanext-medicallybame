import random
import pkg_resources
from ckan.common import config

MAX_TRIALS = 10
BANNER_COUNT = 4


def medicallybame_version():
    ext_name = 'ckanext-medicallybame'
    packages = pkg_resources.require(ext_name)
    return packages[0].version


def medicallybame_banner_url():
    trial = 0
    image_url = None

    while not image_url and trial < MAX_TRIALS:
        index = random.randint(1, BANNER_COUNT)
        field = 'medicallybame_bg_image_url_%s' % index
        image_url = config.get(field)
        trial += 1

    return image_url
