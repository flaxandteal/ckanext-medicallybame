def accepted_image_mime_types():
    return [
        'jpg',
        'jpeg',
        'gif',
        'png',
    ]


def accepted_mime_types():
    return [
        'html',
        'json',
        'xml',
        'text',
        'csv',
        'xls',
        'api',
        'pdf',
        'zip',
        'rdf',
        'nquad',
        'ntriples',
        'turtle',
        'shp',
    ] + accepted_image_mime_types()
