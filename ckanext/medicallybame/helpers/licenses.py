from ckan import model as model


def medbame_license_options(license_id=None):
    # return list of licenses to support use of `license_ids` field
    register = model.Package.get_license_register()
    sorted_licenses = sorted(register.values(), key=lambda x: x.title)
    return sorted_licenses


def id_belongs_to_license(id, license):
    has_ids = hasattr(license, 'legacy_ids')
    return (id == license.id) or (has_ids and id in license.legacy_ids)


def get_license(_id):
    for license in medbame_license_options():
        if id_belongs_to_license(_id, license):
            return license
    return None


def get_license_title(license_id):
    for license in medbame_license_options():
        if id_belongs_to_license(license_id, license):
            return license.title
    return None
