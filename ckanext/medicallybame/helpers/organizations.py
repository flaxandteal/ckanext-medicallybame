from ckan import logic
from ckan.lib import search
from ckan.lib import helpers as ckan_helpers


def get_organizations_list():
    return logic.get_action('group_list')({}, {'type': 'organization'})


def get_pkg_extra(pkg, keyname):
    if 'extras' in pkg and pkg['extras']:
        for extra in pkg['extras']:
            if extra['key'] == keyname:
                return extra['value']
    return None


def organizations_basic_info(separate_children_from_parents=False):
    def convert_organization_to_dict(organization, depth):
        current_organization = {}
        organization_id = organization.pop('id')
        current_organization['id'] = organization_id
        current_organization['name'] = organization.pop('name')
        current_organization['title'] = organization.pop('title')

        # if depth=0, org is not a child of another
        current_organization['depth'] = depth

        current_organization['own_package_count'] = \
            organizations_that_have_packages.pop(organization_id, 0)

        _info = ckan_organizations_info.pop(current_organization['name'], {})
        own_available_package_count = _info.get('count', 0)
        children_data_dict = generate_children_data(
            organization.pop('children'), depth
        )

        current_organization['children'] = children_data_dict['dict_children']
        current_organization['total_package_count'] = \
            children_data_dict['current_total_package_count'] + \
            current_organization['own_package_count']

        current_organization['available_package_count'] = \
            children_data_dict['current_available_package_count'] + \
            own_available_package_count

        current_organization['active'] = \
            current_organization['name'] == organization_in_request

        current_organization['display'] = \
            not organization_in_request or current_organization['active']

        return current_organization

    def generate_children_data(group_tree_children, depth):
        dict_children = []
        current_available_package_count = 0
        current_total_package_count = 0
        for child in group_tree_children:
            converted_child = convert_organization_to_dict(child, depth + 1)
            dict_children.append(converted_child)
            current_available_package_count += \
                converted_child.get('available_package_count', 0)

            current_total_package_count += \
                converted_child.get('total_package_count', 0)

        return {
            'dict_children': dict_children,
            'current_available_package_count': current_available_package_count,
            'current_total_package_count': current_total_package_count
        }

    # get organizations
    organizations = get_organizations_list()
    ckan_organizations_info = {
        item['name']: item
        for item in ckan_helpers.get_facet_items_dict('organization')
    }

    # get orgs with datasets, and datasets count
    query = search.PackageSearchQuery()
    q = {
        'facet.field': ['groups', 'owner_org'],
        'facet.limit': -1,
        'fl': 'groups',
        'rows': 1,
        'q': '+capacity:public',
    }

    query.run(q)
    organizations_that_have_packages = query.facets.get('owner_org')

    # transform each org into dict for use and include other info
    organizations_data = []
    organization_in_request = ckan_helpers.get_request_param('organization')
    for organization in organizations:
        current_organization = convert_organization_to_dict(organization, 0)
        organizations_data.append(current_organization)

    if separate_children_from_parents:
        return organizations_info_with_children_as_separate(organizations_data)
    return organizations_data


def organizations_info_with_children_as_separate(organizations):
    all_organizations = []
    for organization in organizations:
        children = organization.get('children', [])
        if children:
            for child in children:
                child['parent_name'] = organization['name']

            all_organizations.extend(
                organizations_info_with_children_as_separate(children)
            )
        all_organizations.append(organization)
    return all_organizations
