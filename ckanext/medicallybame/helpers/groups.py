from ckan import logic
from ckan.lib import helpers as ckan_helpers


def fetch_groups():
    data_dict_page_results = {
        'all_fields': True,
        'type': 'group',
        'limit': None,
        'offset': 0,
    }
    return logic.get_action('group_list')({}, data_dict_page_results)


def get_faceted_groups(items_limit=None):
    groups = fetch_groups()
    facets = ckan_helpers.get_facet_items_dict(facet='groups', limit=items_limit)
    facets_by_name = {}
    for facet in facets:
        facets_by_name[facet['name']] = facet
    for group in groups:
        group_name = group['name']
        if group_name in facets_by_name:
            group['facet_active'] = facets_by_name[group['name']]['active']
            group['facet_count'] = facets_by_name[group['name']]['count']
        else:
            group['facet_active'] = False
            group['facet_count'] = 0
    return groups
