from routes.mapper import SubMapper


class BAMERouter:

    def __init__(self, route_map):
        ctrl_path = 'ckanext.medicallybame.controllers'

        self.route_map = route_map
        self.home_ctrl = '%s.home:HomeController' % (ctrl_path)
        self.user_ctrl = '%s.user:UserController' % (ctrl_path)
        self.orgs_ctrl = '%s.organization:OrganizationController' % (ctrl_path)

    def set_routes(self):
        self._connect_home()
        self._connect_orgs()
        self._connect_user()
        self._redirects()

    def _connect_home(self):
        with SubMapper(self.route_map, controller=self.home_ctrl) as m:
            m.connect('/', action='index')
            m.connect('/contact-us', action='contact')

    def _connect_orgs(self):
        with SubMapper(self.route_map, controller=self.orgs_ctrl) as m:
            m.connect('/organization', action='index')

    def _connect_user(self):
        with SubMapper(self.route_map, controller=self.user_ctrl) as m:
            m.connect('/user', action='index')
            m.connect('/drafts', action='drafts')

    def _redirects(self):
        kwargs = {'_redirect_code': '301 Moved Permanently'}
        self.route_map.redirect('/group', '/', **kwargs)
